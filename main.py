# textAdventure - hi, thehappiecat! hi, everyone else! my name is atropos148
# and this is my text adventure game for the reddit challenge! hope you will like it!

import re
from locFile import loc

# instructions are a single string
instructions = ('Hello! Welcome to the text adventure game by atropos148!'
                '\n In this game you will be in and around a mysterious house.'
                '\n  Controls: type the number next to the choice you want to make and press Enter. Like this:')

# setup
inventory = []
frontDoorBroken = 0
frontDoorUnlocked = 0
firstAnswer = 0
dinerCooked = 0
livingTV = 0
health = 1


divider = '+------+------+'
gameOver = ("\nYou died! Your body was found 3 hours later by one of the women living in this house. "
            "\n Several of the big news stations made a story about it, stating that this was an 'attack' "
            "and that everyone should make sure their homes are 'safe'.")
win = ("\n You managed to steal the book! Now you can finally find some REAL friends! Not just cheap 'smart' bots. "
       "\n Powerful friends that will help you achieve anything you want. As you drink the third beer, "
       "siting on the roof of nearly collapsed house on the outskirts of the city, you start planing your new life.")


# FUNctions

# curLoc means currentLocation
def moveOn(msg, location, curLoc):
    choice = '1 Yes\n2 No'
    print('Do you want to go ' + msg)
    print(divider)
    print(choice)
    print(divider)

    answerTest = int(input())

    if answerTest == 1:
        curLoc = location  # for a while i didn't realize i can put loc['garden'] inside location and  use it like that
        return curLoc
    elif answerTest == 2:
        curLoc = curLoc  # second choice is to stay where you are
        return curLoc

def mainMenu(firstAns):
    while not firstAns == 1:
        print(divider)
        print(instructions)  # first print instructions so player knows what to do
        print(divider)
        print('1 Play the game')
        print(divider)
        try:
            firstAns = int(input())  # wait for input so i can be sure player read through the instructions
        except ValueError:
            print('That is not a numbre, hombre!')  # typo turned into a joke, that is the kind of stuff i like to do


###
# START
###

mainMenu(firstAnswer)

curLoc = loc['frontDoor']
print(divider)
print("Your mission is to find and extract a book, only known as 'Codex of Broken Syntax'.")
input()

try:  # big gameplay loop
    while True:  # runs until code breaks out of it

        # DEAD
        if health < 1:
            print(gameOver)
            input()
            inventory = []
            frontDoorBroken = 0
            frontDoorUnlocked = 0
            firstAnswer = 0
            dinerCooked = 0
            livingTV = 0
            health = 1
            mainMenu(firstAnswer)
            curLoc = loc['frontDoor']

        # front door check
        if frontDoorBroken == 1 and curLoc == loc['frontDoor']:
                curLoc = loc['frontDoorBroken']

        # main reason I use lists, it makes it easy to print the right description, choices and answers

        ###
        # generic code needed at all locations
        ###

        # print description
        print(divider)
        print(curLoc['desc'])
        print(divider)

        # print choices
        numOfAns = 0
        listOfnumOfAns = []
        for choice in curLoc['choices']:
            numOfAns += 1
            print(str(numOfAns) + '. ' + choice)
            listOfnumOfAns.append(numOfAns)
        print(divider)

        # input is taken as integer to simplify code later, most of input is collected here

        inputRegex = re.compile(r'\d')

        while True:
            while True:
                inputChoice = input()
                answer = inputRegex.search(inputChoice)
                if answer is not None:
                    break
                else:
                    print('You must type in a number')

            inputChoiceInt = int(inputChoice)
            if inputChoiceInt not in listOfnumOfAns:
                print('Wrong number, try again!')
            else:
                break
        print(divider)

        # print answer
        ans = inputChoiceInt - 1
        print('> ' + curLoc['answers'][ans])
        input()

        ###
        # location specific code
        ###

        # FRONTDOOR
        if curLoc == loc['frontDoor']:

            if inputChoiceInt == 3:
                frontDoorBroken = 1
                msg = 'inside?'
                location = loc['shoeRoom']
                locCheck = moveOn(msg, location, curLoc)
                curLoc = locCheck

            elif inputChoiceInt == 4:
                if 'key' in inventory:
                    print('You already stole the key.')
                else:
                    print('However, there was a key taped to the bottom of it. '
                          'You save the small key from this horrible life, by taking it with you on an adventure.')
                    input()
                    inventory.append('key')

            elif inputChoiceInt == 5:
                if frontDoorUnlocked == 1:
                    curLoc = loc['shoeRoom']
                else:
                    if 'key' in inventory:
                        print('Your superior intelligence allows you to unlock the door. The key seems happy about it.')
                        input()
                        frontDoorUnlocked = 1
                        msg = 'inside?'
                        location = loc['shoeRoom']
                        locCheck = moveOn(msg, location, curLoc)
                        curLoc = locCheck
                    else:
                        print('Seems like you are missing something.')
                        input()

            elif inputChoiceInt == 6:
                msg = 'to the garden?'
                location = loc['garden']
                locCheck = moveOn(msg, location, curLoc)
                curLoc = locCheck

            elif inputChoiceInt == 7:
                if 'book' in inventory:
                    print('Time to get outta here!')
                    break
                else:
                    print('You feel like you are missing something.')
                    input()

        # FRONTDOORBROKEN
        elif curLoc == loc['frontDoorBroken']:

            if inputChoiceInt == 3:
                curLoc = loc['shoeRoom']

            elif inputChoiceInt == 4:
                msg = 'to the garden?'
                location = loc['garden']
                locCheck = moveOn(msg, location, curLoc)
                curLoc = locCheck

            elif inputChoiceInt == 5:
                if 'book' in inventory:
                    print('Time to get outta here!')
                    break
                else:
                    print('You feel like you are missing something.')
                    input()

        # SHUEROOM
        elif curLoc == loc['shoeRoom']:

            if inputChoiceInt == 3:
                msg = 'through the door?'
                location = loc['mainRoom']
                locCheck = moveOn(msg, location, curLoc)
                curLoc = locCheck

            elif inputChoiceInt == 4:
                    frontDoorUnlocked = 1
                    curLoc = loc['frontDoor']

        # MAINROOM
        elif curLoc == loc['mainRoom']:
            if inputChoiceInt == 1:
                curLoc = loc['livingRoom']
            elif inputChoiceInt == 2:
                curLoc = loc['kitchen']
            elif inputChoiceInt == 3:
                curLoc = loc['shoeRoom']
            elif inputChoiceInt == 4:
                curLoc = loc['upstairsMain']
            elif inputChoiceInt == 6:
                curLoc = loc['storage']
            elif inputChoiceInt == 7:
                curLoc = loc['shoeRoom']

        # LIBRARY
        elif curLoc == loc['library']:
            if inputChoiceInt == 1:
                inventory.append('book')
            elif inputChoiceInt == 3:
                curLoc = loc['upstairsMain']

        # GARDEN
        elif curLoc == loc['garden']:
            if inputChoiceInt == 1:
                curLoc = loc['gardenFountain']
            elif inputChoiceInt == 2:
                msg = 'through the door?'
                location = loc['storage']
                locCheck = moveOn(msg, location, curLoc)
                curLoc = locCheck
            elif inputChoiceInt == 3:
                curLoc = loc['frontDoor']

        # GARDENFOUNTAIN
        elif curLoc == loc['gardenFountain']:
            if inputChoiceInt == 1:
                health = 0
                print("However, the liquid burns your stomach as well. "
                      "After about 10 seconds of massive pain you pass out. "
                      "Last thought you have is 'Did I turn off the oven?'")
            elif inputChoiceInt == 2:
                curLoc = loc['garden']

        # KITCHEN
        elif curLoc == loc['kitchen']:
            if inputChoiceInt == 1:
                if dinerCooked == 0:
                    dinerCooked = 1
                    print('There is enough, so you start to prepare healthy, but filling meal. '
                          'After some time, you put the finished plates on the table, half expecting '
                          'your kids to start eating.')
                else:
                    print('There is already a cooked diner on the table. It smells good.')
            elif inputChoiceInt == 2:
                inventory.append('big knife')
            elif inputChoiceInt == 3:
                curLoc = loc['mainRoom']

        # LIVING ROOM
        elif curLoc == loc['livingRoom']:
            if inputChoiceInt == 1:
                if livingTV == 0:
                    livingTV = 1
                    print("TV flicks to life. The National News is on, reporter is talking about a 'rare' case "
                          "of police bots smashing a 'criminals' car. You always knew the CopBot factories "
                          "ware shady business, can't expect much from former drug lord.")
                    input()
                elif livingTV == 1:
                    print('Turning off the TV is the only way to not go insane. At least your brain works like that.')
                    input()
            elif inputChoiceInt == 4:
                curLoc = loc['mainRoom']

        # UPSTAIRS
        elif curLoc == loc['upstairsMain']:
            if inputChoiceInt == 1:
                curLoc = loc['bedroom']
            elif inputChoiceInt == 2:
                health = 0
                print("The last thing you hear is a record of some Chinese man saying 'You dead!'. "
                      "After that the heat of explosion consumes your body. Congrats!")
            elif inputChoiceInt == 3:
                curLoc = loc['library']
            elif inputChoiceInt == 5:
                curLoc = loc['mainRoom']

        # BEDROOM
        elif curLoc == loc['bedroom']:
            if inputChoiceInt == 3:
                curLoc = loc['upstairsMain']

        # STORAGE
        elif curLoc == loc['storage']:
            if inputChoiceInt == 1:
                if 'canned food' not in inventory:
                    print('You put some cans into your backpack.')
                    input()
                    inventory.append('canned food')
                else:
                    print('You already have enough.')
                    input()
            elif inputChoiceInt == 2:
                curLoc = loc['mainRoom']
            elif inputChoiceInt == 3:
                curLoc = loc['garden']

except SyntaxError:
    print('Syntax happened :(')

print(win)
input()

if 'big knife' in inventory:
    print('The big knife you took from the kitchen helped you scare many crazy humans. '
          'Pathetic little creatures. You took so much money from them.')
    input()
if 'canned food' in inventory:
    print('5 cans of food. That is how much you took. Most of it is edible without heating it up first. '
          '\n Useful, considering yu are on the run again. Always trying to find better life.')
    input()
print('Thank you for playing my game. Hope you had fun. '
      'Any feedback is welcome on Reddit: just message atropos148. That is me. Have a nice day.')
input()
