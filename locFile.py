# here is a dictionary of locations, where each location has a ordered list of possible actions and another ordered list of answers for easier use later
# loc is short for locations(duh, obviously)



loc = {'frontDoor':{'desc':"You are standing at the front door.",
                          'choices':['Knock on the door',
                                     'Look at the door',
                                     'Kick the door down',
                                     'Pick up the doormat',
                                     'Open the door',
                                     'Look around',
                                     'Leave'],
                            'answers':['There is a sudden scream from inside the house.',
                                         'Massive wooden front door stand in your way to entering this house.',
                                         'No matter how thick, this wooden door has no chance of surviving your metal leg. The hole you made is big enough to walk through.',
                                         'You want to put the doormat in your pocket, but it is too big. What a shame.',
                                         'You stare at the door. It stares back.',
                                         'After a brief look, you found a small garden door. It seems to be unlocked.',
                                         'Your bike is waiting for you just ouside the house.'
                                ],
                    'name':'frontDoor'
                          },
             
             'frontDoorBroken':{'desc':'You are standing at a hole in the wall, formerly known as front door.',
                                'choices':['Knock on the doorframe',
                                           'Look at the remains of the door',
                                           'Go inside',
                                           'Look around',
                                           'Leave'],
                                'answers':['There is a sudden scream from inside the house.',
                                           'Frame of the massive wooden front door shows you a way to enter this house.',
                                           'You walk into the house through stylish new door.',
                                           'After a brief look, you found a small garden door. It seems to be unlocked.',
                                           'Your bike is waiting for you just ouside the house.'
                                    ],
                                'name':'frontDoorBroken'
                                },

             'garden':{'desc':'Lush garden spreads as far as your eyes can see.',
                       'choices':['Explore the garden',
                                  'Stay close to the house',
                                  'Go back to the front of the house'],
                       'answers':['Following the path deeper into the garden leads you to a beautiful fountain. Chisseled into white rock are many creatures, most half human, half animal.',
                                  'Walking next to a wall of the house you find a blue door.',
                                  'No time for gardening right now.'
                           ],
                       'name':'garden'
                       },
             
             'gardenFountain':{'desc':'Fountain is keeping you company.',
                               'choices':['Drink the liquid from the fountain',
                                          'Walk back'],
                               'answers':['This water tastes...like trouble. It burns your throat when you swalow. Fixn Paste will take care of any damage. Hopefully.',
                                          'Better go back before you lose track.'
                                   ],
                               'name':'gardenFountain'
                               },

             'shoeRoom':{'desc':'Shoes are lining the walls of this room. Thankfully you do not need any. Two doors lead out of this room.',
                         'choices':['Count the shoes',
                                    'Open the small door',
                                    'Open the large door',
                                    'Go back outside'],
                         'answers':['There are exactly 241 shoes in this room. You feel proud that it took you only 1.6 seconds to count them.',
                                    'What is this, a door for ants? It is too small for your advanced body.',
                                    "Words 'Truth Awaits' is written on the door. Wonder what that means.", #using "double quotes " here allows for 'single quotes' in the acutal ingame text
                                    'You walk out the door. Just like when she took your batteries.'
                             ],
                         'name':'shoeRoom'
             },
       
             'mainRoom':{'desc':'Many candles cast shadows all over the walls. Kitchen and library are both visible from here. Also, you can walk up the stairs, visit the toilet,go to the shoe room or open the door with a skull mark on it.',
                         'choices':['Walk to the living room',
                                    'Get some food',
                                    'Go to the shoe room',
                                    'Go up the stairs',
                                    'Check the toilet',
                                    'Go through the skull door',
                                    'Go look at the shoes'],
                         'answers':['Your legs carry you closer to room of the living. Or is it the room of the dead?',
                                    'As you walk to the kitchen, your mind imagines the kinds of meals people in this house eat each day. Damn Family subroutines are acting up again.',
                                    'Your legs carry you to the ShueLand.',
                                    'The staris are steeper than they look.', # yeeep #STARI forever
                                    "Just a normal toilet room. Cleaner than most you encountered.",
                                    'Hopefully, there are no traps in there.',
                                    'You walk to the Shoe Room. Or is it Show Room?' # top tier joke, much proud of this one  
                             ],
                         'name':'mainRoom'
                         },
       
             'library':{'desc':'Map of this room visualises under your legs. It helps a lot in finding the book.',
                        'choices':['Grab the book',
                                   'Look around',
                                   'Go back outside'],
                        'answers':["Here it is in it's full glory, 'Codex of Broken Syntax' or 'Necronomicon: Your friend Cthulhu' as it says on the back. Nice, heavy and totaly yours. Better get back home now.",
                                   'Even though you know where the book is, you decide to walk around a bit and find some rare books to take as extras. However, nothing cathches your attention.',
                                   'You leave tons of paper behind your back.',    
                            ],
                        'name':'library'
                        },
       
             'kitchen':{'desc':'This is the cleanest kitchen you ever saw.',
                        'choices':['Prepare a diner for three people',
                                   'Loot all the cupboards',
                                   'Walk back to the hallway'],
                        'answers':['You scan the kitchen for ingredients.',
                                   'Other than food, you found a very big knife with ornaments engraved in it. You put it in the backpack.',
                                   'You have no need for food.'
                                   ],
                        'name':'kitchen'
                        },
       
             'livingRoom':{'desc':'There is a peacefull atmosphere in the air.',
                            'choices':['Push a butotn on the TV',
                                       'Lay down on the couch',
                                       'Read the newspaper',
                                       'Go back'
                                ],
                            'answers':['You press a button on the big rectangle on the wall.',
                                       'Couch is as comfy as it looks. Not suprising, this whole house must have cost a lot of money.',
                                       "'VR Has Jobs For Everyone' says the headline of the main article. Written by Ron Gerbalt. Slimy bastard.",
                                       "Enough living for now. Let's get back on track."
                                ],
                            'name':'living room'
                 },
               'upstairsMain':{'desc':'Standing in upstairs hallway, you can see three doors.',
                               'choices':['Open the left door',
                                          'Choose the middle door',
                                          'Go through the right door',
                                          'Examine the Doors',
                                          'Go downstairs'
                                          ],
                               'answers':['You walk into a bedroom.',
                                          'Hope no one minds you snooping around.',
                                          'As you open the door, you see hundereds of books.',
                                          "There is a hole in the left door that lets you see a bedroom inside, middle door has a sticker saying 'Enter only if allowed, not safe for life' and the right door is plain white door.",
                                          'You descend.'],
                               
                               'name':'upstairsMain'
                   },
       
               'bedroom':{'desc':'You are getting sleepy just looking at the bed.',
                            'choices':['Loot the wardrobe',
                                       'Lay down on the bed',
                                       'Leave'
                                ],
                            'answers':["It's full of dresses and military clothing. Sadly, not your size.",
                                       'You are almost floating when you lay down on this bed. If only there was a way to take it with you.',
                                       "Well that was productive. Let's move on."
                                ],
                            'name':'bedroom'
                 },

               'storage':{'desc':'This room is packed with canned food and other survival gear. Cool stuff. There is a blue door nad a door with a skull on it.',
                            'choices':['Take some food',
                                       'Go out the skull door',
                                       'Go out the blue door'
                                ],
                            'answers':["You quickly look over the various food cans.",
                                       'You swear the skull is laughing at you.',
                                       "This door is a bit creaky."
                                ],
                            'name':'bedroom'
                 },
             }
